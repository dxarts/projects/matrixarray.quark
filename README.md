## MatrixArray.quark : Read Me
_A two-dimensional array structure to facilitate fast(er) matrix operations._


### Installing

Install via SuperCollider's command line:

>`Quarks.install("https://gitlab.com/dxarts/projects/matrixarray.quark")`


### Feedback and Bug Reports

Known issues are logged at
[GitLab](https://gitlab.com/dxarts/projects/matrixarray.quark/issues).


### Change log

0.1.4
- fix: -det correct value for singular matricies

0.1.3
- refactor: avoid inline warning -det

0.1.2
- Deprecate (with warning): -zeroWithin
- Add: -thresh2

0.1.1
- Migrate repository from [GitHub](https://github.com/mtmccrea/MatrixArray.quark) to [GitLab](https://gitlab.com/dxarts/projects/matrixarray.quark)

0.1.0
- initial release.

### Credits / Attribution

This is a partial refactoring of the `Matrix` class from the
[MathLib](https://github.com/supercollider-quarks/MathLib) quark
originally authored by sc.solar, Till Bovermann, Christofer Fraunberger,
and Dan Stowell.

This refactoring includes a much faster determinant calculation based on
[LU Decomposition](https://en.wikipedia.org/wiki/LU_decomposition).

The algorithm is published in:
[Intel Application Notes AP-931, Streaming SIMD Extensions - LU Decomposition](http://web.archive.org/web/20150701223512/http://download.intel.com/design/PentiumIII/sml/24504601.pdf)

The development of the `MatrixArray` Quark for SuperCollider3 is supported
by
[The University of Washington's Center for Digital Arts and Experimental Media (DXARTS)](https://dxarts.washington.edu/).
&nbsp;

### Contributors

* Joseph Anderson : (@joslloand)
* Michael McCrea :  (@mtmccrea)
